const api_key = ""

if (!localStorage.getItem("isAuthorized")){
    window.location.href = '/signin/signin.html'
}
function applyFilter() {

    genre_id = document.getElementById("genreList")?.value;

    console.log(genre_id);
    fetchContentFromAPI();
}

window.onload = () => {
    fetchContentFromAPI();
    filterList();
}

function fetchContentFromAPI() {
    getOriginals()
    getTopRated();

}
let genre_id = "";

let fetchMovies = (url, element_selector, path_type) => {
    fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error("something went wrong")
            }
        })
        .then((data) => {
            showMovies(data, element_selector, path_type)
        })
        .catch((error) => {
            console.log(error)
        })
}

let showMovies = (movies, element_selector, path_type) => {
    let moviesEl = document.querySelector(element_selector);
    moviesEl.innerHTML = ""
    for (let movie of movies.results) {
        let image = `
           <span class="modal-content-wrapper"><span class="image-modal-content"><img data-overview="${movie?.overview}" data-lang="${movie?.original_language}" data-rating=${movie?.vote_average} data-name="${movie?.name || movie?.original_title}" src="https://image.tmdb.org/t/p/original${movie[path_type]}" /></span></span>
        `
        moviesEl.innerHTML += image;
    }

}

let getOriginals = () => {
    let url = `https://api.themoviedb.org/3/discover/tv?api_key=${api_key}&with_genres=${genre_id|| "" }`

    fetchMovies(url, '.netflixOriginals__movies', 'poster_path')
}


let getTopRated = () => {
    let url = "https://api.themoviedb.org/3/movie/top_rated?api_key=" + api_key + "&language=en-US&page=1"
    fetchMovies(url, '#topRated', 'backdrop_path')
}

function filterList() {
    const url = "https://api.themoviedb.org/3/genre/movie/list?api_key=" + api_key + "&page=1";
    fetch(url)
        .then(response => response.json())
        .then(data => {


            const filterList = document.getElementById("genreList");
            data?.genres?.forEach(title =>
                filterList.insertAdjacentHTML('beforeend', optionListTemplate(title)))
        })
        .catch(error => console.log(error));

    const optionListTemplate = ({
        id,
        name
    }) => (`<option  value=${id}>${name}</option>`)
}


function AddPopUpClass() {
    const lightboxImages = document.querySelectorAll('.image-modal-content img');

    const modalElement = element =>
        document.querySelector(`.image-modal-popup ${element}`);

    const body = document.querySelector('body');

    document.addEventListener('click', () => {
        body.style.overflow = 'auto';
        modalPopup.style.display = 'none';
    });

    const modalPopup = document.querySelector('.image-modal-popup');

    lightboxImages.forEach(img => {
        const data = img.dataset;
        img.addEventListener('click', e => {
            body.style.overflow = 'hidden';
            e.stopPropagation();
            modalPopup.style.display = 'block';
            modalElement('h1').innerHTML = `${data.name} - ${data.lang}`;
            modalElement('p').innerHTML = `${data.overview} \n vote- ${data.rating}⭐`
            modalElement('img').src = img.src;
        });
    });
}
setInterval(function () {
    AddPopUpClass();
}, 2000);