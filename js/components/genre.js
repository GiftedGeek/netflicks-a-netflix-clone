const url = "https://api.themoviedb.org/3/genre/movie/list?api_key=04c35731a5ee918f014970082a0088b1&page=1";
fetch(url)
    .then(response => response.json())
    .then(data => {


        const filterList = document.getElementById("genereList"); data?.genres?.forEach(title =>
            filterList.insertAdjacentHTML('beforeend', optionListTemplate(title)))
    })
    .catch(error => console.log(error));

const optionListTemplate = ({ id, name }) => (`<option value=${id}>${name}</option>`)